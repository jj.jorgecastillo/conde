import React from 'react';

import { Result, Button } from 'antd';

const Result500 = () => {
  return <div className="Result">
    <Result
      status="500"
      title="500"
      subTitle="Sorry, something went wrong."
      extra={<Button type="primary">Back Home</Button>}
    />
  </div>
};

export default Result500;
