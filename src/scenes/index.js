import Home from './Home';
import Contact from './Contact';
import About from './About';
import Tabla from './Tabla';
import Entidades from './Entidades';
import Client from './Client';
import Log from './Log';

export { Home, Contact, About, Tabla, Entidades, Client, Log };
