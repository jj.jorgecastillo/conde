import React, { useState } from 'react';
import { Form, Upload } from 'antd';

const PicturesWall = () => {
  const [fileList, setFileList] = useState([]);

  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };

  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  return (
    <Form.Item
      name="upload"
      label="Comprobante"
      valuePropName="fileList"
      getValueFromEvent={normFile}
      extra="Máximo 3 Comprobantes"
      rules={[{ required: true, message: 'Por favor, subir mínimo 1 comprobante' }]}
      hasFeedback
    >
      <Upload listType="picture-card" onChange={onChange} onPreview={onPreview}>
        {fileList.length < 1 && '+ Agregar'}
      </Upload>
    </Form.Item>
  );
};

export default PicturesWall;
